package com.example.p2_rickmortyapi.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.p2_rickmortyapi.R
import com.example.p2_rickmortyapi.data.model.DataItem
import com.example.p2_rickmortyapi.room.Character
import com.example.p2_rickmortyapi.room.CharacterApplication
import com.example.p2_rickmortyapi.ui.view.CharacterListFragment
import com.example.p2_rickmortyapi.ui.view.CharacterListFragmentDirections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.ListViewHolder>() {
    var characters = mutableListOf<DataItem>()

    fun setCharactersList(characterList: List<DataItem>) {
        characters = characterList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.character_item, parent, false)
        return ListViewHolder(v)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(characters[position])
    }

    override fun getItemCount(): Int = characters.size

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var image: ImageView
        private var fullName: TextView
        private var star: ImageView

        init {
            image = itemView.findViewById(R.id.image_character_list)
            fullName = itemView.findViewById(R.id.name_item)
            star = itemView.findViewById(R.id.star)
        }

        fun bindData(character: DataItem) {
            Glide.with(itemView).load(character.imageUrl).into(image)
            fullName.text = character.fullName
            star.setOnClickListener {
                character.fav = !character.fav
                if (character.fav){
                    star.setImageResource(R.drawable.star_on)
                    CoroutineScope(Dispatchers.Main).launch {
                        withContext(Dispatchers.IO){
                            CharacterApplication.dataBase.characterDao().addContact(
                                Character(character.id)
                            )
                        }
                    }
                } else {
                    star.setImageResource(R.drawable.star_off)
                    CoroutineScope(Dispatchers.Main).launch {
                        withContext(Dispatchers.IO){
                            CharacterApplication.dataBase.characterDao().deleteContact(
                                Character(character.id)
                            )
                        }
                    }
                }
            }

            itemView.setOnClickListener {
                val direction = CharacterListFragmentDirections.actionCharacterListFragmentToCharacterDetailFragment(character.id)
                Navigation.findNavController(itemView).navigate(direction)
            }
        }
    }
}

package com.example.p2_rickmortyapi.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.p2_rickmortyapi.data.model.DataItem
import com.example.p2_rickmortyapi.data.repository.Repository
import com.example.p2_rickmortyapi.room.Character
import com.example.p2_rickmortyapi.room.CharacterApplication
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CharacterViewModel : ViewModel() {
    private val repository = Repository()
    val characters = MutableLiveData<List<DataItem>>()
    val characters2 = mutableListOf<DataItem>()
    var fav = mutableListOf<Character>()

    fun getCharacterList() {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                fav.clear()
                fav.addAll(CharacterApplication.dataBase.characterDao()
                    .getFavCharacters())
            }
        }
        val call = repository.getCharactersList()
        call.enqueue(object : Callback<List<DataItem>> {
            override fun onResponse(
                call: Call<List<DataItem>>,
                response: Response<List<DataItem>>
            ) {
                for (d in response.body()!!){
                    for (f in fav){
                        if (d.id == f.id){
                            d.fav = true
                        }
                    }
                }
                Log.d("API RESPONSE", "GET BODY : ${response.body()}")
                characters.postValue(response.body())
                characters2.addAll(response.body()!!)
            }

            override fun onFailure(call: Call<List<DataItem>>, t: Throwable) {
                Log.d("API RESPONSE", t.message.toString())
            }
        })
    }
    fun getFavCharacters() = characters2
}

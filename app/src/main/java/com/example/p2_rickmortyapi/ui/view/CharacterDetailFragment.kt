package com.example.p2_rickmortyapi.ui.view

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.p2_rickmortyapi.R
import com.example.p2_rickmortyapi.data.model.DataItem
import com.example.p2_rickmortyapi.ui.adapter.CharacterAdapter
import com.example.p2_rickmortyapi.ui.viewModel.CharacterViewModel

class CharacterDetailFragment: Fragment(R.layout.character_detail) {

    //LAYOUT VARIABLES
    private lateinit var fullName: TextView
    private lateinit var firstName: TextView
    private lateinit var lastName: TextView
    private lateinit var title: TextView
    private lateinit var family: TextView
    private lateinit var imageView: ImageView

    //VIEW MODEL
    private val viewModel: CharacterViewModel by activityViewModels()


    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        imageView = view.findViewById(R.id.image_character)
        fullName = view.findViewById(R.id.fullname_text)
        firstName = view.findViewById(R.id.firstname_text)
        lastName = view.findViewById(R.id.lastname_text)
        title = view.findViewById(R.id.title_text)
        family = view.findViewById(R.id.family_text)

        viewModel.getCharacterList()

        viewModel.characters.observe(viewLifecycleOwner, {
            Glide.with(view).load(it[arguments?.getInt("id")!!].imageUrl).into(imageView)
            fullName.text = it[arguments?.getInt("id")!!].fullName
            firstName.text = "First Name: " +  it[arguments?.getInt("id")!!].firstName
            lastName.text = "Last Name: " +  it[arguments?.getInt("id")!!].lastName
            title.text = "Title: " +  it[arguments?.getInt("id")!!].title
            family.text = "Family: " +  it[arguments?.getInt("id")!!].family
        })
    }
}

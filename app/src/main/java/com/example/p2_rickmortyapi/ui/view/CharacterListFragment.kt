package com.example.p2_rickmortyapi.ui.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.p2_rickmortyapi.R
import com.example.p2_rickmortyapi.data.model.DataItem
import com.example.p2_rickmortyapi.ui.adapter.CharacterAdapter
import com.example.p2_rickmortyapi.ui.viewModel.CharacterViewModel


class CharacterListFragment : Fragment(R.layout.character_list) {

    //RECYCLER VIEW
    private lateinit var recyclerView: RecyclerView

    //VIEW MODEL
    private val viewModel: CharacterViewModel by activityViewModels()

    //ADAPTER
    private val adapter = CharacterAdapter()

    private lateinit var favs: ImageView
    private lateinit var normal: ImageView

    //ON VIEW CREATED
    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //IDs
        recyclerView = view.findViewById(R.id.recycler_view)

        viewModel.getCharacterList()
        favs = view.findViewById(R.id.fav_list)
        normal = view.findViewById(R.id.normal_list)

        favs.setOnClickListener {
           adapter.characters = viewModel.characters2.filter { it.fav }.toMutableList()
           adapter.notifyDataSetChanged()
        }
        normal.setOnClickListener{
            adapter.characters = viewModel.characters2
            adapter.notifyDataSetChanged()
        }

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        viewModel.characters.observe(viewLifecycleOwner, {
            adapter.setCharactersList(it)
        })
    }
}

package com.example.p2_rickmortyapi.data.model

import com.google.gson.annotations.SerializedName

data class DataItem(
    @SerializedName("id") val id: Int,
    @SerializedName("fullName") val fullName: String,
    @SerializedName("firstName") val firstName: String,
    @SerializedName("lastName") val lastName: String,
    @SerializedName("family") val family: String,
    @SerializedName("title") val title: String,
    @SerializedName("image") val image: String,
    @SerializedName("imageUrl") val imageUrl: String,
    var fav: Boolean = false
)
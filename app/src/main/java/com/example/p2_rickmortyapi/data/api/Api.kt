package com.example.p2_rickmortyapi.data.api

import com.example.p2_rickmortyapi.data.model.DataItem
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {

    //GETTER
    @GET("Characters")
    fun getCharactersList(): Call<List<DataItem>>

    companion object {
        private var BASE_URL = "https://thronesapi.com/api/v2/"
        //API CALL
        private var retrofitServices: Api? = null
        fun getInstance(): Api{
            if (retrofitServices == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitServices = retrofit.create(Api::class.java)
            }
            return retrofitServices!!
        }
    }
}

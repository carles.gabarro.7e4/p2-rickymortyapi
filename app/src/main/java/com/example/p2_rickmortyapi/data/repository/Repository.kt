package com.example.p2_rickmortyapi.data.repository

import com.example.p2_rickmortyapi.data.api.Api

class Repository{
    private val apiService = Api.getInstance()
    fun getCharactersList() = apiService.getCharactersList()

}
package com.example.p2_rickmortyapi.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CharacterDAO {
    @Query("SELECT * FROM Character")
    fun getFavCharacters(): MutableList<Character>

    @Insert
    fun addContact(character: Character)

    @Delete
    fun deleteContact(character: Character)

}
package com.example.p2_rickmortyapi.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Character")
data class Character (
    @PrimaryKey var id: Int

)
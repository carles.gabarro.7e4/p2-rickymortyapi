package com.example.p2_rickmortyapi.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Character::class], version = 1)
abstract class CharacterDataBase: RoomDatabase(){
    abstract fun characterDao(): CharacterDAO
}
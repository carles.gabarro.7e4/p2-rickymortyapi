package com.example.p2_rickmortyapi.room

import android.app.Application
import androidx.room.Room

class CharacterApplication: Application() {

    companion object{
        lateinit var dataBase: CharacterDataBase
    }

    override fun onCreate() {
        super.onCreate()
        dataBase = Room.databaseBuilder(this,
            CharacterDataBase::class.java,
        "CharacterDatabase").build()
    }

}